import authReducer from './reducers/authReducer';
import reducer from './reducers/reducer'

import { combineReducers } from 'redux';
const rootReducer = combineReducers({
   
    bar: reducer,
    auth: authReducer,
  });
  

export default rootReducer