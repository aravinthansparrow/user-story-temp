import { configureStore } from '@reduxjs/toolkit';
import axios from 'axios';
import rootReducer from './rootReducer';


import { setCreated, setApproved, setRejected, setUnApproved} from './actions/actions';


const store = configureStore({
  reducer: rootReducer
});

// Fetch the initial data and update the state
axios.get('http://localhost:3002/clients')
  .then((response) => {
    const clients = response.data;

    const rejectedEstimates = clients.filter((client) => client.status === 'rejected');
    const notApprovedEstimates = clients.filter((client) => client.status !== 'approved');
    const approvedEstimates = clients.filter((client) => client.status === 'approved');
    const createdCount = clients.length;
    const rejectedCount = rejectedEstimates.length;
    const notApprovedCount = notApprovedEstimates.length


    // Count the number of approved estimates

    const approvedCount = approvedEstimates.length;
    console.log(createdCount);
    console.log(approvedCount)
    console.log(rejectedCount)
    console.log(notApprovedCount);


  

    // Dispatch the actions to update the initial state values
    store.dispatch(setCreated(createdCount));
    store.dispatch(setApproved(approvedCount));
    store.dispatch(setRejected(rejectedCount));
    store.dispatch(setUnApproved(notApprovedCount));
  })
  .catch((error) => {
    console.error('Error fetching initial data:', error);
  });





export default store;
