
export const loginSuccess = ( username, id, email, role) => ({
    type: 'LOGIN_SUCCESS',
    payload: {username, id, email, role} ,
  });
 

  export const logout = () => ({
    type: 'LOGOUT',
  });
  