export const setCreated = (count) => {
  return {
    type: 'SET_CREATED',
    payload: count,
  };
};

export const setApproved = (count) => {
  return {
    type: 'SET_APPROVED',
    payload: count,
  };
};

export const setRejected = (count) => {
  return {
    type: 'SET_REJECTED',
    payload: count,
  };
};


export const setUnApproved = (count) => {
  return {
    type: 'SET_UNAPPROVED',
    payload: count,
  };
};
