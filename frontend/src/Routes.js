import Login from '../src/pages/Login'
import SidenavBar from './pages/SidenavBar'
import BarGraph from '../src/pages/BarGraph'
import  ComponentType  from '../src/pages/ComponentType'
import ForgotPassword from '../src/pages/ForgotPassword'
import Header from './pages/Header'
import Estimation from './pages/Estimation'
import ActivityPercentage from "./pages/ActivityPercent"
import Complexity from "./pages/Complexity"
import EstimateList from "./pages/EstimateList"
import ChangePassword from "./pages/ChangePassword"
export {
    Login,
    SidenavBar,
    BarGraph,
    ComponentType,
    ForgotPassword,
    Header,
    Estimation,
    ActivityPercentage,
    Complexity,
    EstimateList,
    ChangePassword
}