import React from "react";
import { Route, Routes, BrowserRouter, Navigate } from "react-router-dom"; // Import Navigate
import BarGraph from "./pages/BarGraph";
import EstimateList from "./pages/EstimateList";
import Complexity from "./pages/Complexity";
import ActivityPercent from "./pages/ActivityPercent";
import ComponentType from "./pages/ComponentType";
import ClientForm from "./pages/Estimation";
import Layout from "./pages/Dashboard";
import Login from "./pages/Login";
import EstimateSummary from "./pages/EstimateSummary";
import SmallEstimateList from "./pages/smallEstimationList";
import TopClients from "./pages/TopClients";
import  CircularProgress  from "./pages/ProgressBar";
import GeneralSettings from "./pages/GeneralSettings";
import ChangePassWord from "./pages/ChangePassword";


function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Routes>
          <Route path="login" element={<Login />} />
          <Route path="/" element={<Layout/>}>
            <Route path="/" element={<Navigate to="/login" replace />} />
            <Route path="dashboard" element={<BarGraph />} />
            <Route path="complexity" element={<Complexity />} />
            <Route path="component-type" element={<ComponentType />} />
            <Route path="generate-estimation" element={<ClientForm />} />

            <Route path="estimation-list" element={<EstimateList />} />
            <Route path="activity-percentage" element={<ActivityPercent />} />
            <Route
              path="/estimation-list/:clientId"
              element={<EstimateSummary />}
            />
            <Route
              path="/generate-estimate/:clientId"
              element={<EstimateSummary />}
            />
            <Route path="login" element={<Login />} />
            <Route path="settings" element={<GeneralSettings/>}/>
            <Route path="/Changepassword" element={<ChangePassWord/>}/>
            
          </Route>
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;