const initialState = {
    
    username: localStorage.getItem('username'),
    id: localStorage.getItem('id'),
    email: localStorage.getItem('email'),
    role: localStorage.getItem('role'),
    isAuthenticated: false,
  };
  
  const authReducer = (state = initialState, action) => {
    switch (action.type) {
      case 'LOGIN_SUCCESS':
        const { username, id , email, role} = action.payload;
        console.log(username)
        console.log(id)
        console.log(email)
        
        localStorage.setItem('username', username);
        localStorage.setItem('id', id);
        localStorage.setItem('email', email);
        localStorage.setItem('role', role);
        return {
          ...state,
        
          username,id,email,role,
          isAuthenticated: true,
        };
      case 'LOGOUT':
        
        localStorage.removeItem('username');
        localStorage.removeItem('id');
        localStorage.removeItem('email')
        localStorage.removeItem('role')
        return {
          ...state,
          
          username: null,
          isAuthenticated: false,
        };
      default:
        return state;
    }
  };
  
  export default authReducer;
  