const initialState = {
  created: 0,
  approved: 0,
  rejected: 0, 
  notapproved:0,
  
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case 'SET_CREATED':
      return {
        ...state,
        created: action.payload,
      };
    case 'SET_APPROVED':
      return {
        ...state,
        approved: action.payload,
      };
      case 'SET_REJECTED':
      return {
        ...state,
        rejected: action.payload,
      };
      case 'SET_UNAPPROVED':
      return {
        ...state,
        notapproved: action.payload,
      };
    default:
      return state;
  }
};

export default reducer;
