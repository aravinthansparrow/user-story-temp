import React, { useEffect } from "react";
import { useSelector } from "react-redux";
import { Bar } from "react-chartjs-2";
import { Chart, registerables } from "chart.js";
import "../styles/BarGraph.css";
import NumberCards from "./Cards";
import SmallEstimateList from "./smallEstimationList";
import TopClients from "./TopClients";

Chart.register(...registerables);

const BarGraph = () => {
  useEffect(() => {});

  const username = useSelector((state) => state.auth.username);
  const user = username.charAt(0).toUpperCase() + username.slice(1);
  // Access the 'created' and 'approved' values from the Redux store
  const reduxCreated = useSelector((state) => state.bar.created);
  const reduxApproved = useSelector((state) => state.bar.approved);

  const data = {
    labels: ["Created", "Approved"],
    datasets: [
      {
        label: "Count",
        data: [reduxCreated, reduxApproved],
        backgroundColor: ["red", "limegreen"],
        width: "300",
        barThickness: 30,
      },
    ],
  };

  const options = {
    scales: {
      y: {
        beginAtZero: true,
        ticks: {
          stepSize: 1,
        },
      },
    },
    plugins: {
      legend: {
        display: false, // Hide legend
      },
      tooltip: {
        enabled: true, // Disable tooltip
      },
    },
    interaction: {
      mode: "index",
      intersect: false,
    },
  };

  return (
    <div>
      <h2 className="user-name">Welcome, {user}!</h2>
      <div className="wrap-flex">
        <NumberCards />
        <TopClients />
      </div>
      <div className="divide-section">
      <SmallEstimateList />
      <div className="chart-container">
        <h2>No of Estimation</h2>
        <div className="chart-wrapper">
          <Bar data={data} options={options} />
        </div>
      </div>
      
      </div>
    </div>
  );
};

export default BarGraph;
