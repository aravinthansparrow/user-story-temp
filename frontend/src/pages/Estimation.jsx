import React, { useState } from 'react';
import axios from 'axios';
import WorkItem from './WorkItem';
import '../styles/Estimation.css';
import { useDispatch, useSelector } from "react-redux";



const ClientForm = () => {

  const dispatch = useDispatch();
  const username = useSelector((state) => state.auth.username);

  const [formData, setFormData] = useState({
    clientName: '',
    clientAddress: '',
    email: '',
    createdBy: username,
  });

  const [showPopup, setShowPopup] = useState(false);
  const [showWorkItem, setShowWorkItem] = useState(false);
  const [clientId, setClientId] = useState(null);

  const handleChange = (event) => {
    const { name, value } = event.target;
    setFormData((prevData) => ({
      ...prevData,
      [name]: value,
    }));
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    try {
      // Send a POST request to the backend to create a new client
      console.log(formData)
      const response = await axios.post('http://localhost:3002/clients', formData);
       const id = response.data.id;
      
       setClientId(id);
     
      // Show the popup
      setShowPopup(true);
      // Reset the form after submission
      setFormData({
        clientName: '',
        clientAddress: '',
        email: '',
        createdBy: ''
        
      });
      console.log(formData)
      
   
      // toast.success('Client details updated')
    } catch (error) {
      console.error('Error creating client:', error);
    }
  };

  const handlePopupClose = () => {
    // Hide the popup
    setShowPopup(false);
    // Show the WorkItem component
    setShowWorkItem(true);
  };

  return (
    <div>
      {!showWorkItem && (
        <div>
          <h2 className="heading-title">Generate Estimation</h2>
          <form onSubmit={handleSubmit} className="client-form">
            <div>
              <center>
                <p className="client-title">Set client details</p>
              </center>
            </div>
            <div>
              <label htmlFor="clientName">Client Name: </label>
              <input
                type="text"
                id="clientName"
                name="clientName"
                value={formData.clientName}
                onChange={handleChange}
                required
              />
            </div>
            <div>
              <label htmlFor="clientAddress">Client Address: </label>
              <textarea
                id="clientAddress"
                name="clientAddress"
                value={formData.clientAddress}
                onChange={handleChange}
                required
                rows={4} 
                cols={50}
              />
            </div>
            <div>
              <label htmlFor="email">Email ID: </label>
              <input
                type="email"
                id="email"
                name="email"
                value={formData.email}
                onChange={handleChange}
                required
              />
            </div>
            <center>
              <button type="submit">Save</button>
            </center>
          </form>
          {showPopup && (
            <div className="popup">
              <div className="popup-content">
                <p>Client Details Updated</p>
                <button className="popup-button" onClick={handlePopupClose}>OK</button>
              </div>
            </div>
          )}
        </div>
      )}
      {showWorkItem && <WorkItem clientId={clientId} />}
    </div>
  );
};

export default ClientForm;
