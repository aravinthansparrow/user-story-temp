import React, { useState, useEffect } from 'react';
import axios from 'axios';
import PeopleAltRoundedIcon from '@mui/icons-material/PeopleAltRounded';
import "../styles/Clients.css"
import StarsIcon from '@mui/icons-material/Stars';

const TopClients = () => {
  const [topClientNames, setTopClientNames] = useState([]);

  useEffect(() => {
    fetchClients();
  }, []);

  const fetchClients = async () => {
    try {
      const response = await axios.get('http://localhost:3002/clients');
      const allClients = response.data;

      // Count the occurrence of each client name
      const clientNameCounts = allClients.reduce((counts, client) => {
        const { clientName } = client;
        counts[clientName] = (counts[clientName] || 0) + 1;
        return counts;
      }, {});

      // Sort the client names based on their occurrence
      const sortedClientNames = Object.keys(clientNameCounts).sort(
        (a, b) => clientNameCounts[b] - clientNameCounts[a]
      );

      // Get the top 3 most repeated client names
      const topClientNames = sortedClientNames.slice(0, 3);

      setTopClientNames(topClientNames);
    } catch (error) {
      console.error('Error fetching clients:', error);
    }
  };

  return (
    <div className='client-part'>
      <h2 className='client-titles'>Top Clients<PeopleAltRoundedIcon/></h2>
      <ul className='client-names'>
        {topClientNames.map((clientName, index) => (
          <li className='top-names' key={index}><StarsIcon/>{clientName}</li>
        ))}
      </ul>
    </div>
  );
};

export default TopClients;
