import React, { useEffect, useState } from "react";
import TextField from "@mui/material/TextField";
import Button from "@mui/material/Button";
import { Box, Container, Typography } from "@mui/material";
import VisibilityIcon from "@mui/icons-material/Visibility";
import VisibilityOffIcon from "@mui/icons-material/VisibilityOff";
import ErrorOutlineIcon from "@mui/icons-material/ErrorOutline";
import axios from "axios";
import "../styles/ChangePassword.css";
import { useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
const ChangePassWord = () => {
  const [password, setPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");
  const [showPassword, setShowPassword] = useState(false);
  const [showConfirmPassword, setShowConfirmPassword] = useState(false);
  const [passwordChanged, setPasswordChanged] = useState(false);
  const [passwordChangeSuccess, setPasswordChangeSuccess] = useState(false);
  const [passwordError, setPasswordError] = useState("");
  const [passwordInvalid, setPasswordInvalid] = useState(false);

  const id = useSelector((state) => state.auth.id);
  const navigate = useNavigate();

  const handleConfirmPasswordChange = (e) => {
    setConfirmPassword(e.target.value);
  };

  const toggleShowPassword = () => {
    setShowPassword(!showPassword);
  };

  const toggleShowConfirmPassword = () => {
    setShowConfirmPassword(!showConfirmPassword);
  };

  const handleSubmit = async () => {
    if (password === null || password.trim() === "") {
      setPasswordError("Please enter a valid password");
      setPasswordChanged(false);
      setPasswordChangeSuccess(false);
    } else if (!isPasswordValid(password)) {
      setPasswordError("Please enter a valid password");
      setPasswordChanged(false);
      setPasswordChangeSuccess(false);
    } else {
      try {
        const response = await axios.put(`http://localhost:3002/user/${id}`, {
          newPassword: password,
        });

        if (response.status === 200) {
          setPasswordChanged(true);
          setPasswordChangeSuccess(true);
          setPasswordError("");
          console.log("Password changed!");
          setTimeout(() => {
            navigate("/login");
          }, 1200);
          
          
        } else {
          throw new Error(response.data.message);
        }
      } catch (error) {
        setPasswordError(error.message);
        setPasswordChanged(false);
        setPasswordChangeSuccess(false);
      }
    }
  };

  const isPasswordValid = (password) => {
    const passwordRegex =   /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%?&])[A-Za-z\d@$!%?&]{8,}$/
    return passwordRegex.test(password);
  };

  const handlePasswordChange = (e) => {
    const newPassword = e.target.value;
    setPassword(newPassword);
    setPasswordInvalid(!isPasswordValid(newPassword));
  };

  return (
    <div className="root">
        <Box className="content">
          {passwordChangeSuccess && (
            <Typography
              variant="body1"
              align="center"
              className="success-message"
              style={{ color: "green" }}
            >
              Password changed successfully!
            </Typography>
          )}

          <Typography variant="h5" className="pwd-title"  gutterBottom>
            Change Password
          </Typography>
          <p className="pwd-hint">
            Hint : Password should contain Numbers with Lower and Upper character
          </p>
          <div>
            <Box sx={{ position: "relative" }}>
              <TextField
                label="New Password"
                type={showPassword ? "text" : "password"}
                value={password}
                onChange={handlePasswordChange}
                fullWidth
                margin="normal"
                className="MuiFormControl-root label-title"
                InputProps={{
                  endAdornment: (
                    <>
                      <Button
                        onClick={toggleShowPassword}
                        className="visibility-button"
                        style={{
                          position: "absolute",
                          right: 10,
                          top: 10,
                          color: "black",
                        }}
                      >
                        {showPassword ? (
                          <VisibilityIcon style={{ color: "rgb(37, 167, 170)" }}  />
                        ) : (
                          <VisibilityOffIcon style={{ color: "rgb(37, 167, 170)" }}  />
                        )}
                      </Button>
                    </>
                  ),
                }}
              />

              <TextField
                label="Confirm Password"
                type={showConfirmPassword ? "text" : "password"}
                value={confirmPassword}
                onChange={handleConfirmPasswordChange}
                fullWidth
                margin="normal"
                className="MuiFormControl-root label-title"
                InputProps={{
                  endAdornment: (
                    <Button
                      onClick={toggleShowConfirmPassword}
                      className="visibility-button"
                      style={{
                        position: "absolute",
                        right: 10,
                        bottom: 10,
                        color: "black",
                      }}
                    >
                      {showConfirmPassword ? (
                        <VisibilityIcon style={{ color: "rgb(37, 167, 170)" }} />
                      ) : (
                        <VisibilityOffIcon style={{ color: "rgb(37, 167, 170)" }}  />
                      )}
                    </Button>
                  ),
                }}
              />
            </Box>
          </div>
          {passwordError && (
            <Typography
              variant="body1"
              align="center"
              className="error-message"
            >
              <ErrorOutlineIcon style={{ marginRight: 10, marginBottom: -5 }} />
              {passwordError}
            </Typography>
          )}

          <div >
            <Button className="buttonBox" onClick={handleSubmit} variant="contained">
              Submit
            </Button>
          </div>
        </Box>
    </div>
  );
};

export default ChangePassWord;