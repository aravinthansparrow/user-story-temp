import axios from "axios";
import { useState, useEffect } from "react";
import '../styles/GeneralSettings.css'; 
import "../styles/EstimateSummary.css"


const GeneralSettings = () => {
  const [generalSettings, setGeneralSettings] = useState({});
  const [modal, setModal] = useState(false);
  const [values, setValues] = useState({});

  useEffect(() => {
    getGeneralSettings();
  }, []);

  const getGeneralSettings = async () => {
    try {
      const response = await axios.get('http://localhost:3002/generalsettings');
      const data = response.data;
      setGeneralSettings(data);
      setValues(data); // Set initial values
      console.log(data);
      // Process the userData as needed
    } catch (error) {
      console.error('Error retrieving user:', error);
    }
  };

  const handleUpdate = () => {
    setModal(true);
  };

  const handleClose = () => {
    setModal(false);
  };

  const handleChange = (e) => {
    setValues((prevState) => ({
      ...prevState,
      [e.target.name]: e.target.value
    }));
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    const updatedValues = { ...values };
    console.log(updatedValues);
    try {
      const response = await axios.put('http://localhost:3002/generalsettings', updatedValues);
      if (response.status === 204) {
        setModal(false);
        setGeneralSettings(updatedValues); // Update the display table with updated values
      }
    } catch (error) {
      console.error('Error updating general settings:', error);
    }
  };

  return (
    <div className="table-overall">
      <h2 className="estimate-head">General Settings</h2>
      <table className="work-item-table">
        <tbody>
          <tr className="workitem-tab">
            <td>
              <label>Version</label>
            </td>
            <td>
              <input name="version" value={generalSettings.version || ''} readOnly />
            </td>
          </tr>
          <tr className="workitem-tab">
            <td>
              <label>Document Version</label>
            </td>
            <td>
              <input name="document_version" value={generalSettings.document_version || ''} readOnly />
            </td>
          </tr>
          <tr className="workitem-tab">
            <td>
              <label>Hours per Story Point</label>
            </td>
            <td>
              <input name="hours_per_story_point" value={generalSettings.hours_per_story_point || ''} readOnly />
            </td>
          </tr>
          <tr className="workitem-tab">
            <td>
              <label>Rate per Hour</label>
            </td>
            <td>
              <input name="rate_per_hour" value={generalSettings.rate_per_hour || ''} readOnly />
            </td>
          </tr>
        </tbody>
      </table>
      <button className="buttonBox" onClick={handleUpdate}>Update</button>
      
      {modal && (
        <div className="modal-overlay">
          <div className="modal">
            <h2 className="estimate-head" >Update General Settings</h2>
            <form onSubmit={handleSubmit}>
              <div>
                <label style={{color:"white"}} >Version:</label>
                <input style={{marginTop:"10px"}} name="version" value={values.version || ''} onChange={handleChange} />
              </div>
              <div>
                <label style={{color:"white"}} >Document Version:</label>
                <input style={{marginTop:"10px"}} name="document_version" value={values.document_version || ''} onChange={handleChange} />
              </div>
              <div>
                <label style={{color:"white"}} >Hours per Story Point:</label>
                <input style={{marginTop:"10px"}} name="hours_per_story_point" value={values.hours_per_story_point || ''} onChange={handleChange} />
              </div>
              <div>
                <label style={{color:"white"}} >Rate per Hour:</label>
                <input style={{marginTop:"10px"}} name="rate_per_hour" value={values.rate_per_hour || ''} onChange={handleChange} />
              </div>
              <div className="modal-buttons">
                <button type="submit">Submit</button>
                <button type="button" onClick={handleClose}>Cancel</button>
              </div>
            </form>
          </div>
        </div>
      )}
    </div>
  );
};

export default GeneralSettings;
