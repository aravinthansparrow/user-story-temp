import React, { useState } from "react";
import { Box, useMediaQuery } from "@mui/material";
import { Outlet } from "react-router-dom";
import Homepage from "./SidenavBar";
import "../styles/mainrender.css"



const Layout = () => {

  return (
    <Box>
      
     <div style={{display:"flex"}}>
        <Homepage/>
        <div className="render-component">
          <Outlet/>
        </div>
      </div>
    </Box>
  );
};

export default Layout;