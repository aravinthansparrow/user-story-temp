import React, { useState, useEffect } from 'react';
import Modal from 'react-modal';
import EditIcon from '@mui/icons-material/Edit';
import DeleteIcon from '@mui/icons-material/Delete';
import axios from 'axios';
import AddCircleOutlineIcon from '@mui/icons-material/AddCircleOutline';
import "../styles/ComponentType.css";
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { ToastContainer } from 'react-toastify';

const ComponentType = () => {
  const [components, setComponents] = useState([]);
  const [newComponent, setNewComponent] = useState('');
  const [newComponentModalIsOpen, setNewComponentModalIsOpen] = useState(false);
  const [editModalIsOpen, setEditModalIsOpen] = useState(false);
  const [selectedComponentIndex, setSelectedComponentIndex] = useState(null);
  const [updatedComponentName, setUpdatedComponentName] = useState('');
  const [defaultComponent, setDefaultComponent] = useState(null);
  const [confirmModalIsOpen, setConfirmModalIsOpen] = useState(false);
  const [confirmComponentIndex, setConfirmComponentIndex] = useState(null);
 
  useEffect(() => {
    fetchComponents();
  }, []);

  const fetchComponents = async () => {
    try {
      const response = await axios.get('http://localhost:3002/components');
      setComponents(response.data);
      const defaultComponentIndex = response.data.findIndex(component => component.default === 'default');
      setDefaultComponent(defaultComponentIndex);
      console.log(defaultComponent)
    } catch (error) {
      console.error('Failed to fetch components', error);
    }
  };
  const addComponent = async () => {
    if (newComponent.trim() !== '') {
      try {
        const response = await axios.post('http://localhost:3002/components', {
          name: newComponent,
          isDefault: defaultComponent === null || components.length === 0,
        });
        const newComponentObj = response.data;
        setComponents([...components, newComponentObj]);
        setNewComponent('');
        setNewComponentModalIsOpen(false);
        if (newComponentObj.isDefault) {
          setDefaultComponent(components.length);
        }
      } catch (error) {
        console.error('Failed to add component', error);
      }
    }
  };

  const updateComponentName = async () => {
    if (selectedComponentIndex !== null && updatedComponentName.trim() !== '') {
      try {
        const componentToUpdate = components[selectedComponentIndex];
        const updatedComponent = {
          ...componentToUpdate,
          name: updatedComponentName.trim(),
          isDefault: componentToUpdate.isDefault || selectedComponentIndex === defaultComponent,
        };
        await axios.put(`http://localhost:3002/components/${componentToUpdate.id}`, updatedComponent);
        const updatedComponents = [...components];
        updatedComponents[selectedComponentIndex] = updatedComponent;
        setComponents(updatedComponents);
        if (updatedComponent.isDefault) {
          setDefaultComponent(selectedComponentIndex);
        }
        closeEditModal();
      } catch (error) {
        console.error('Failed to update component', error);
      }
    }
  };

  const deleteComponent = async (index) => {
    try {
      const componentToDelete = components[index];
      await axios.delete(`http://localhost:3002/components/${componentToDelete.id}`);
      const updatedComponents = [...components];
      updatedComponents.splice(index, 1);
      setComponents(updatedComponents);
      if (index === defaultComponent) {
        setDefaultComponent(null);
      }
  
      toast.success('Component deleted successfully!', {
        autoClose: 2000, // Close the toast after 3000 milliseconds (3 seconds)
      });
    } catch (error) {
      console.error('Failed to delete component', error);
    }
  };
  
  // toast.configure();

  const handleAddComponent = () => {
    setNewComponentModalIsOpen(true);
  };

  const handleEditComponentName = () => {
    updateComponentName();
  };

  const openEditModal = (index) => {
    setSelectedComponentIndex(index);
    setUpdatedComponentName(components[index].name);
    setEditModalIsOpen(true);
  };

  const closeEditModal = () => {
    setEditModalIsOpen(false);
    setSelectedComponentIndex(null);
    setUpdatedComponentName('');
  };

  const handleSetDefault = (index) => {
    setConfirmComponentIndex(index);
    setConfirmModalIsOpen(true);
  };

  const handleConfirmDefault = async () => {
    try {
      const componentToUpdate = components[confirmComponentIndex];
      const updatedComponent = {
        ...componentToUpdate,
        isDefault: true,
      };
      console.log(updatedComponent)

      await axios.put(`http://localhost:3002/components/${componentToUpdate.id}`, updatedComponent);

      const updatedComponents = [...components];
      updatedComponents[confirmComponentIndex] = updatedComponent;
      setComponents(updatedComponents);
      setDefaultComponent(confirmComponentIndex);
      setConfirmModalIsOpen(false);
    } catch (error) {
      console.error('Failed to update component default status', error);
    }
  };

  const handleCancelConfirm = () => {
    setConfirmComponentIndex(null);
    setConfirmModalIsOpen(false);
  };


  return (
    <div className="component-manager">
      <div className='component-title'>
        <h2>Component Type</h2>
        <button className="add-component-button" onClick={handleAddComponent}>
          Add Component
          <AddCircleOutlineIcon/>
        </button>
      </div>
      {components.map((component, index) => (
        <div className='component-items' key={index}>
          <div className={`component ${defaultComponent === index ? 'default' : ''}`}>
            <p>{component.name}</p>
            <div className='set-define'>
              {defaultComponent === index ? (
                <span className="default-badge">Default</span>
              ) : (
                <button
                  className="default-button"
                  onClick={() => handleSetDefault(index)}
                >
                  Set as Default
                </button>
              )}
              <button className="edit-button" onClick={() => openEditModal(index)}>
                <EditIcon sx={{ color: 'black' }} />
              </button>
              <button className="delete-button" onClick={() => deleteComponent(index)}>
                <DeleteIcon sx={{ color: 'black' }} />
              </button>
            </div>
          </div>
        </div>
      ))}

      {/* Add New Component Modal */}
      <Modal
        isOpen={newComponentModalIsOpen}
        onRequestClose={() => setNewComponentModalIsOpen(false)}
        contentLabel="Add New Component"
        className="modal"
        overlayClassName="modal-overlay"
      >
        <div className="modal-content">
          <h2>Add New Component</h2>
          <input
            type="text"
            value={newComponent}
            onChange={(e) => setNewComponent(e.target.value)}
          />
          <button className="modal-button" onClick={addComponent}>Add</button>
          <button className="modal-button" onClick={() => setNewComponentModalIsOpen(false)}>Cancel</button>
        </div>
      </Modal>

      {/* Edit Component Modal */}
      <Modal
        isOpen={editModalIsOpen}
        onRequestClose={closeEditModal}
        contentLabel="Edit Component Name"
        className="modal"
        overlayClassName="modal-overlay"
      >
        <div className="modal-content">
          <h2>Edit Component Name</h2>
          <input
            type="text"
            value={updatedComponentName}
            onChange={(e) => setUpdatedComponentName(e.target.value)}
          />
          <button className="modal-button" onClick={handleEditComponentName}>Save</button>
          <button className="modal-button" onClick={closeEditModal}>Cancel</button>
        </div>
      </Modal>

      {/* Set Default Confirmation Modal */}
      <Modal
        isOpen={confirmModalIsOpen}
        onRequestClose={handleCancelConfirm}
        contentLabel="Set Default Confirmation"
        className="modal"
        overlayClassName="modal-overlay"
      >
        <div className="modal-content">
          <h2>Set as Default</h2>
          <p>Are you sure you want to set this component as the default?</p>
          <button className="modal-button" onClick={handleConfirmDefault}>Yes</button>
          <button className="modal-button" onClick={handleCancelConfirm}>No</button>
        </div>
      </Modal>
      <ToastContainer />
    </div>
  );
};

export default ComponentType;