import React, { useState, useEffect } from "react";
import axios from "axios";
import { useSelector, useDispatch } from "react-redux";
import {
  setCreated,
  setApproved,
  setUnApproved,
  setRejected,
} from "../actions/actions";
import { useNavigate } from "react-router-dom";
import EstimateSummary from "./EstimateSummary";
import Modal from "react-modal";
import SearchIcon from "@mui/icons-material/Search";
import "../styles/EstimateList.css";
import VisibilityIcon from "@mui/icons-material/Visibility";

const EstimateList = () => {
  const [clients, setClients] = useState([]);
  const [selectedClientId, setSelectedClientId] = useState(null);
  const [showEstimateSummary, setShowEstimateSummary] = useState(false);
  const [filterByDate, setFilterByDate] = useState("");
  const [filterByClientName, setFilterByClientName] = useState("");
  const [filterByEstimatedBy, setFilterByEstimatedBy] = useState("");
  const [filterByStatus, setFilterByStatus] = useState("");
  const [filteredClients, setFilteredClients] = useState([]);
  const [showFilters, setShowFilters] = useState(true);
  const [showConfirmationModal, setShowConfirmationModal] = useState(false);
  const [confirmationAction, setConfirmationAction] = useState("");
  const reduxCreated = useSelector((state) => state.created);
  const reduxApproved = useSelector((state) => state.approved);

  // Dispatch the actions to update the 'created' and 'approved' values in the Redux store
  const dispatch = useDispatch();
  const navigate = useNavigate();

  useEffect(() => {
    fetchClients();
  }, []);

  useEffect(()=>{
    applyFilters();
  }, [clients, filterByDate, filterByClientName, filterByEstimatedBy, filterByStatus])

  useEffect(() => {
    // Count the number of rejected and not approved estimates
    const rejectedEstimates = clients.filter(
      (client) => client.status === "rejected"
    );
    const notApprovedEstimates = clients.filter(
      (client) => client.status !== "approved"
    );
    const approvedEstimates = clients.filter(
      (client) => client.status === "approved"
    );
    const createdCount = clients.length;
    const rejectedCount = rejectedEstimates.length;
    const notApprovedCount = notApprovedEstimates.length;

    // Count the number of approved estimates
    const approvedCount = approvedEstimates.length;

    dispatch(setCreated(createdCount));
    dispatch(setApproved(approvedCount));
    dispatch(setRejected(rejectedCount));
    dispatch(setUnApproved(notApprovedCount));
  }, [clients, dispatch]);

  const fetchClients = async () => {
    try {
      const response = await axios.get("http://localhost:3002/clients");
      setClients(response.data);
      setFilteredClients(response.data);
    } catch (error) {
      console.error("Error fetching clients:", error);
    }
  };

  const handleView = (clientId) => {
    setSelectedClientId(clientId);
    setShowEstimateSummary(true);
    setShowFilters(false);
    navigate(`/estimation-list/${clientId}`);
  };

  const handleBack = () => {
    setShowEstimateSummary(false);
    setShowFilters(true);
  };

  const applyFilters = () => {
    let filteredData = clients;
    if(filterByDate){
      filteredData = filteredData.filter((client) => 
      client.createdAt.substring(0, 10).includes(filterByDate))
    }
    if(filterByClientName){
      filteredData = filteredData.filter((client) => 
      client.clientName.toLowerCase().includes(filterByClientName.toLowerCase()))
    }
    if(filterByEstimatedBy){
      filteredData = filteredData.filter((client) => 
      client.createdBy.toLowerCase().includes(filterByEstimatedBy.toLowerCase()))
    }

    if(filterByStatus){
      filteredData = filteredData.filter((client) => 
      client.status.toLowerCase().includes(filterByStatus.toLowerCase()))
    }
    setFilteredClients(filteredData);
  }

  const handleStatusUpdate = (clientId, status) => {
    setSelectedClientId(clientId);
    setConfirmationAction(status);
    setShowConfirmationModal(true);
  };

  const handleConfirmation = async (clientId) => {
    try {
      await axios.put(`http://localhost:3002/clients/${clientId}`, {
        status: confirmationAction,
      });

      // Update the status in the clients and filteredClients states
      const updatedClients = clients.map((client) => {
        if (client.id === clientId) {
          return { ...client, status: confirmationAction };
        }
        return client;
      });

      const updatedFilteredClients = filteredClients.map((client) => {
        if (client.id === clientId) {
          return { ...client, status: confirmationAction };
        }
        return client;
      });

      setClients(updatedClients);
      setFilteredClients(updatedFilteredClients);
      setShowConfirmationModal(false);
    } catch (error) {
      console.error("Error updating status:", error);
    }
  };

  return (
    <div>
      {showEstimateSummary ? (
        <EstimateSummary clientId={selectedClientId} />
      ) : (
        <div>
          {showFilters && (
            <div className="filters-container">
              <input
                type="text"
                className="input-list"
                name="filterByDate"
                placeholder="Filter by date"
                value={filterByDate}
                onChange={(e) => setFilterByDate(e.target.value)}
              />
              <input
                type="text"
                className="input-list"
                name="filterByClientName"
                placeholder="Filter by client name"
                value={filterByClientName}
                onChange={(e) => setFilterByClientName(e.target.value)}
              />
              <input
                type="text"
                className="input-list"
                name="filterByEstimatedBy"
                placeholder="Filter by estimated by"
                value={filterByEstimatedBy}
                onChange={(e) => setFilterByEstimatedBy(e.target.value)}
              />
              <input
                type="text"
                className="input-list"
                name="filterByStatus"
                placeholder="Filter by status"
                value={filterByStatus}
                onChange={(e) => setFilterByStatus(e.target.value)}
              />
              <button className="input-list search-btn" onClick={applyFilters}>
                Search
                <SearchIcon />
              </button>
            </div>
          )}
          <div>
            <div className="list-headers">
              <div className="head-serial">S.No</div>
              <div className="head-date">Date</div>
              <div className="head-name">Client Name</div>
              <div className="head-by">Estimated By</div>
              <div className="head-status">Status</div>
              <div className="head-action">Actions</div>
              <div className="head-view">View</div>
            </div>

            <div className="list-tabs">
              {filteredClients.map((client, index) => (
                <div className="field-set" key={client.id}>
                  <div className="head-serial index-num">{index + 1}</div>
                  <div className="head-date">
                    {client.createdAt.substring(0, 10)}
                  </div>
                  <div className="head-name">{client.clientName}</div>
                  <div className="head-by">{client.createdBy}</div>
                  <div className="head-status">{client.status}</div>
                  <div className="head-action">
                    {client.status === "approved" ? (
                      <button className="disable-app" disabled>
                        Accepted
                      </button>
                    ) : client.status === "rejected" ? (
                      <button className="disable-rej" disabled>
                        Rejected
                      </button>
                    ) : (
                      <>
                        <button
                          className="app-btn"
                          onClick={() =>
                            handleStatusUpdate(client.id, "approved")
                          }
                        >
                          Accept
                        </button>
                        <button
                          className="rej-btn"
                          onClick={() =>
                            handleStatusUpdate(client.id, "rejected")
                          }
                        >
                          Reject
                        </button>
                      </>
                    )}
                  </div>
                  <div className="head-view align-l-p">
                    <button
                      className="view-btn"
                      onClick={() => handleView(client.id)}
                    >
                      <VisibilityIcon />
                    </button>
                  </div>
                </div>
              ))}
            </div>
          </div>
        </div>
      )}

      {showEstimateSummary && <button onClick={handleBack}>Back</button>}

      <Modal
        className="modal"
        overlayClassName="modal-overlay"
        isOpen={showConfirmationModal}
        onRequestClose={() => setShowConfirmationModal(false)}
      >
        <h2 className="confirm-header">Confirmation</h2>
        <p className="confirm-para">
          Are you sure you want to{" "}
          {confirmationAction === "approved" ? "approve" : "reject"} this
          estimate?
        </p>
        <button
          className="confirm-btn"
          onClick={() => handleConfirmation(selectedClientId)}
        >
          Confirm
        </button>

        <button
          className="cancel-btn"
          onClick={() => setShowConfirmationModal(false)}
        >
          Cancel
        </button>
      </Modal>
    </div>
  );
};

export default EstimateList;
