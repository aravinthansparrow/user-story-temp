import React, { useState, useEffect } from "react";
import axios from "axios";
import EstimateSummary from "./EstimateSummary";
// import "../styles/WorkItem.css";
import { useNavigate } from "react-router-dom";
import DeleteIcon from '@mui/icons-material/Delete';
import { TableContainer, Table, TableHead, TableBody, TableRow, TableCell, Button } from '@mui/material';
import { toast } from "react-toastify";
const WorkItem = ({ clientId }) => {
  const [rows, setRows] = useState([{}]);

  const [showEstimateSummary, setShowEstimateSummary] = useState(false);
  const [componentTypes, setComponentTypes] = useState([]);
  const [complexities, setComplexities] = useState([]);
  const [defaultComponent, setDefaultComponent] = useState('');
  const navigate = useNavigate();

  useEffect(() => {
    fetchComponentTypes();
    fetchComplexities();
  }, []);
  const fetchComponentTypes = async () => {
    try {
      const response = await axios.get("http://localhost:3002/components");
      const fetchedComponentTypes = response.data;
      setComponentTypes(fetchedComponentTypes);
  
      const defaultComponentData = fetchedComponentTypes.find(
        (component) => component.default === 'default'
      );
  
      if (defaultComponentData && defaultComponentData.name) {
        setDefaultComponent(defaultComponentData.name);
        console.log(defaultComponent)
      }
    } catch (error) {
      console.error("Error fetching component types:", error);
    }
  };
  

  const fetchComplexities = async () => {
    try {
      const response = await axios.get("http://localhost:3002/complexity");
      setComplexities(
        response.data.map((item) => ({
          id: item.id,
          complexity: item.complexity,
          days : item.days
        }))
      );
      selectedComplexity = complexities[0].complexity
      selectedBuildEffort = complexities[0].days
      console.log(complexities)
    } catch (error) {
      console.error("Error fetching complexities:", error);
    }
  };
  const buildEffortOptions = [
    { complexity: "Simple", days: 1 },
    { complexity: "Medium", days: 2 },
    { complexity: "Complex", days: 3 },
  ];

  const selectedComplexity =
    complexities.length > 0 ? complexities[0].complexity : "";
  const selectedBuildEffort = selectedComplexity? complexities[0].days: "0"
console.log(selectedBuildEffort)
  const addRow = () => {
    setRows([...rows, {}]);
  };
  useEffect(()=> console.log(selectedBuildEffort))

  const deleteRow = () => {
    setRows((prevRows) => {
      if (prevRows.length > 1) {
        return prevRows.slice(0, prevRows.length - 1);
      }
      return prevRows;
    });
  };

  const handleChange = (event, index) => {
    const { name, value } = event.target;
    setRows((prevRows) => {
      const updatedRows = [...prevRows];
      updatedRows[index][name] = value;

      if (name === "complexity") {

        const selectedComplexity = value;
        console.log(selectedComplexity)
        const selectedBuildEffort =
          selectedComplexity === "simple"
            ? "1"
            : selectedComplexity === "medium"
              ? "2"
              : selectedComplexity === "complex"
                ? "3"
                : "";

        updatedRows[index]["buildEffort"] = selectedBuildEffort;
        
      }

      return updatedRows;
      
    });
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    try {
      let isFormValid = true;

      for (const row of rows) {
        const buildEffort = parseFloat(row.buildEffort) || 0;
        const effortOverride = parseFloat(row.effortOverride) || 0;
        const finalEffort =
          row.effortOverride || row.buildEffort || selectedBuildEffort;

        const workItem = {
          module: row.module || "",
          userType: row.userType || "",
          appType: row.appType || "",
          componentName: row.componentName || "",
          comments: row.comments || "must",
          description: row.description || "",
          componentType: row.componentType || defaultComponent,
          complexity: selectedComplexity || row.complexity,
          buildEffort: row.buildEffort || selectedBuildEffort,
          effortOverride: row.effortOverride || "0",
          finalEffort: finalEffort, // Use the calculated finalEffort
          clientId: clientId,
        };
        console.log(workItem);
        console.log(finalEffort)

        // Check if any of the required fields are empty
        const requiredFields = [
          "module",
          "userType",
          "appType",
          "componentName",
          "comments",
          "description",
          "componentType",
          "complexity",
          "buildEffort",
        ];

        for (const field of requiredFields) {
          if (!workItem[field]) {
            isFormValid = false;
            // Handle the validation error appropriately (e.g., display error messages to the user)
            console.error(`${field} is required`);
          }
        }

        if (isFormValid) {
          // Send a POST request to the backend to save the workItem
          await axios.post("http://localhost:3002/workItems", workItem);
        }
      }

      if (isFormValid) {
        // Show the success message or perform any desired action
        console.log("WorkItems saved successfully");

        // Reset the rows if needed
        setRows([]);
        navigate(`/generate-estimate/${clientId}`);
        setShowEstimateSummary(true);
      }
    } catch (error) {
      console.error("Error saving workItems:", error);
    }
  };
  useEffect(() => {
    // Update finalEffort when buildEffort changes
    setRows((prevRows) =>
      prevRows.map((row) => ({
        ...row,
        finalEffort: row.effortOverride || selectedBuildEffort,
      }))
    );
  }, [selectedBuildEffort]);

  const renderTableHeader = () => {
    const headers = [
      "S.No",
      "Module",
      "User Type",
      "App Type",
      "Component Name",
      "Comments",
      "Description",
      "Component Type",
      "Complexity",
      "Build Effort (in days)",
      "Effort Override (in days)",
      "Final Effort (in days",
      "Action",
    ];

    return headers.map((header, index) => <TableCell key={index}>{header}</TableCell>);
  };

  const renderTableBody = () => {
    return rows.map((row, index) => {
      const buildEffort = parseFloat(row.buildEffort) || 0;
      const effortOverride = parseFloat(row.effortOverride) || 0;
      const finalEffort = effortOverride ? effortOverride : row.buildEffort || selectedBuildEffort; 
      console.log(finalEffort)// Use effortOverride if available, otherwise use buildEffort
      const isLastRow = index === rows.length - 1;
      const showDeleteButton = index !== 0; // Show delete button for all rows except the first row
      const buildEffortOptions = [
        { complexity: "Simple", days: 1 },
        { complexity: "Medium", days: 2 },
        { complexity: "Complex", days: 3 },
      ];

      return (
       
          
       
        <TableRow key={index} className="items-row">
          <td className="workitem-data">
            <div className="col-1">{index + 1}</div>
          </td>
          <td className="workitem-data">
            <input
              className="col-2"
              type="text"
              name="module"
              value={row.module}
              onChange={(e) => handleChange(e, index)}
              required
            />
          </td>
          <td className="workitem-data">
            <input
              className="col-2"
              type="text"
              name="userType"
              value={row.userType}
              onChange={(e) => handleChange(e, index)}
              required
            />
          </td>
          <td className="workitem-data">
            <input
              className="col-3"
              type="text"
              name="appType"
              value={row.appType}
              onChange={(e) => handleChange(e, index)}
              required
            />
          </td>
          <td className="workitem-data">
            <input
              type="text"
              className="col-4"
              name="componentName"
              value={row.componentName}
              onChange={(e) => handleChange(e, index)}
              required
            />
          </td>
          <td className="workitem-data">
            <select
              name="comments"
              className="col-5"
              value={row.comments || "must"}
              onChange={(e) => handleChange(e, index)}
              required
            >
              <option value="must">Must to have</option>
              <option value="good">Good to have</option>
              <option value="nice">Nice to have</option>
            </select>
          </td>
          <td className="workitem-data">
            <textarea
              value={row.description}
              onChange={(e) => handleChange(e, index)}
              className="col-6"
              name="description"
              required
              cols=""
              rows=""
            ></textarea>
          </td>
          <td className="workitem-data">
          <select
              name="componentType"
              className="col-7"
              value={row.componentType || ""}
              onChange={(e) => handleChange(e, index)}
              required
            >
              {defaultComponent && (
                <option value={defaultComponent}>{defaultComponent}</option>
              )}
              {componentTypes
                .filter((type) => type.name !== defaultComponent)
                .map((type) => (
                  <option key={type.id} value={type.name}>
                    {type.name}
                  </option>
                ))}
            </select>
          </td>
          <td className="workitem-data">
            <select
              name="complexity"
              className="col-8"
              value={row.complexity || selectedComplexity}
              onChange={(e) => handleChange(e, index)}
              required
            >
              {complexities.map((complexity) => (
                <option
                  key={complexity.id}
                  value={
                    complexity.complexity === row.complexity
                      ? complexity.complexity
                      : selectedComplexity
                  }
                >
                  {complexity.complexity === row.complexity
                    ? complexity.complexity
                    : selectedComplexity}
                </option>
              ))}
              {selectedComplexity !== "simple" && (
                <option value="simple">Simple</option>
              )}
              {selectedComplexity !== "medium" && (
                <option value="medium">Medium</option>
              )}
              {selectedComplexity !== "complex" && (
                <option value="complex">Complex</option>
              )}
            </select>
          </td>
          <td className="workitem-data">
            <input
              type="text"
              className="col-9"
              name="buildEffort"
              value={row.buildEffort || selectedBuildEffort}
              readOnly
            />
          </td>
          <td className="workitem-data">
            <input
              type="number"
              className="col-10"
              name="effortOverride"
              value={row.effortOverride || ""}
              onChange={(e) => handleChange(e, index)}
            />
          </td>
          <td className="workitem-data">
            <div className="col-10">{finalEffort}</div>
          </td>
          <td className="workitem-data">
            {showDeleteButton ? (
              <button className="delete-row-button col-11" onClick={deleteRow}>
                <DeleteIcon />
              </button>
            ) : (
              <div className="disable-delete">
                <DeleteIcon />{" "}
              </div>
            )}
          </td>
          </TableRow>
      );
    });
  };

  return (
    <div className="work-item-container">
    {!showEstimateSummary && (
      <div>
        <h2 className="workitem-title">Workitem Table</h2>
        <TableContainer>
          <Table className="work-item-table">
            <TableHead>
              <TableRow>{renderTableHeader()}</TableRow>
            </TableHead>
            <TableBody>{renderTableBody()}</TableBody>
          </Table>
        </TableContainer>
        <Button className="add-row-button" onClick={addRow} variant="contained" color="primary">
          Add Row
        </Button>
        <Button className="submit-button" onClick={handleSubmit} variant="contained" color="primary">
          Submit
        </Button>
      </div>
    )}
    {showEstimateSummary && <EstimateSummary clientId={clientId} />}
  </div>
  );
};

export default WorkItem;