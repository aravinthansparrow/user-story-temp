import React, { useEffect, useState } from 'react';

const CircularProgress = ({ value, maxValue }) => {
  const [progress, setProgress] = useState(0);

  useEffect(() => {
    if (progress >= value) {
      return;
    }

    const interval = setInterval(() => {
      setProgress((prevProgress) => prevProgress + 1);
    }, 10);

    return () => {
      clearInterval(interval);
    };
  }, [value, progress]);

  const calculateProgressPercentage = () => {
    return (progress / maxValue) * 100;
  };

  const calculateStrokeDasharray = () => {
    const percentage = (value / maxValue) * 100;
    const dashLength = (percentage / 100) * 283;
    const gapLength = 283 - dashLength;
    return `${dashLength} ${gapLength}`;
  };

  const calculateStrokeDashoffset = () => {
    const percentage = calculateProgressPercentage();
    const offset = 283 - (percentage / 100) * 283;
    return offset;
  };

  return (
    <svg width="100" height="100">
      <circle
        cx="50"
        cy="50"
        r="35"
        fill="transparent"
        stroke="#ccc"
        strokeWidth="6"
      />
      <circle
        cx="50"
        cy="50"
        r="35"
        fill="transparent"
        stroke="black"
        strokeWidth="6"
        strokeDasharray={calculateStrokeDasharray()}
        strokeDashoffset={calculateStrokeDashoffset()}
        strokeLinecap="round"
        style={{
          animation: `${
            progress === value ? 'none' : 'progressAnimation'
          } 1s linear forwards`,
        }}
      />
      <text x="50" y="55" textAnchor="middle">
        {progress}
      </text>
      <style>
        {`
        @keyframes progressAnimation {
          100% {
            stroke-dashoffset: ${calculateStrokeDashoffset()};
          }
        }
        `}
      </style>
    </svg>
  );
};

export default CircularProgress;
