import React, { useState } from "react";
import { useNavigate, Link } from "react-router-dom";
import {
  Box,
  List,
  ListItem,
  ListItemText,
  Icon,
  Collapse,
  Popover,
} from "@mui/material";
import LogoutIcon from '@mui/icons-material/Logout';
import KeyIcon from '@mui/icons-material/Key';
import { ExpandMore, ChevronRight } from "@mui/icons-material";
import BarGraph from "./BarGraph";
import EstimateList from "./EstimateList";
import Estimation from "./Estimation";
import ComponentType from "./ComponentType";
import Complexity from "./Complexity";
import ActivityPercent from "./ActivityPercent";
import DashboardIcon from "@mui/icons-material/Dashboard";
import TableChartIcon from "@mui/icons-material/TableChart";
import ChecklistIcon from "@mui/icons-material/Checklist";
import ViewWeekIcon from "@mui/icons-material/ViewWeek";
import "../styles/MenuBar.css";
import Logo from "../assets/ig-logos.png";
import AccountCircleOutlinedIcon from "@mui/icons-material/AccountCircleOutlined";
import "../styles/Profile.css";
import "../index.css";
import { useSelector } from "react-redux";
import GeneralSettings from "./GeneralSettings";

const Homepage = () => {
  const [open, setOpen] = useState(false);
  const [selectedPath, setSelectedPath] = useState(null);
  const navigate = useNavigate();

  const handleDropdownToggle = () => {
    setOpen((prevOpen) => !prevOpen);
  };
  const username = useSelector((state) => state.auth.username);
  const role = useSelector((state) => state.auth.role);
  const email = useSelector((state) => state.auth.email);

  const [anchorEl, setAnchorEl] = useState(null);
  const isPopoverOpen = Boolean(anchorEl);

  const popoverId = isPopoverOpen ? "profile-popover" : undefined;

  const handleAvatarClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };
  const handleItemClick = (path, isCategory) => {
    setSelectedPath(path);
    if (isCategory) {
      handleDropdownToggle();
    } else {
      navigate(path);
    }
  };

  const navItems = [
    { label: "Dashboard", path: "/dashboard", component: <BarGraph /> },
    {
      label: "Generate Estimation",
      path: "/generate-estimation",
      component: <Estimation />,
    },
    {
      label: "Estimation List",
      path: "/estimation-list",
      component: <EstimateList />,
    },
    {
      label: "Masters",
      category: [
        {
          label: "Component Type",
          path: "/component-type",
          component: <ComponentType />,
        },
        { label: "Complexity", path: "/complexity", component: <Complexity /> },
        {
          label: "Activity Percentage Split",
          path: "/activity-percentage",
          component: <ActivityPercent />,
        },
        {
          label: "General Settings",
          path: "/settings",
          component: <GeneralSettings />,
        },
      ],
    },
  ];

  const renderNavItems = (items) => {
    return items.map((item, index) => (
      <div className="nav-title" style={{ marginBottom: "10px" }} key={index}>
        <ListItem
          style={{ gap: "6px" }}
          button
          onClick={() =>
            handleItemClick(item.path, Array.isArray(item.category))
          }
        >
          {item.label === "Dashboard" && <DashboardIcon />}
          {item.label === "Generate Estimation" && <TableChartIcon />}
          {item.label === "Estimation List" && <ChecklistIcon />}
          {item.label === "Masters" && <ViewWeekIcon />}
          <ListItemText primary={item.label} />
          {Array.isArray(item.category) && (
            <Icon>{open ? <ExpandMore /> : <ChevronRight />}</Icon>
          )}
        </ListItem>
        {Array.isArray(item.category) && (
          <Collapse in={open}>
            <List component="div" style={{ marginLeft: "10px" }} disablePadding>
              {item.category.map((categoryItem, categoryIndex) => (
                <ListItem
                  button
                  key={categoryIndex}
                  sx={{ pl: 4 }}
                  onClick={() => handleItemClick(categoryItem.path, false)}
                >
                  <ListItemText primary={categoryItem.label} />
                </ListItem>
              ))}
            </List>
          </Collapse>
        )}
      </div>
    ));
  };

  const renderComponent = () => {
    const selectedItem = navItems.find((item) => {
      if (item.path === selectedPath) {
        return true;
      }
      if (Array.isArray(item.category)) {
        return item.category.some(
          (categoryItem) => categoryItem.path === selectedPath
        );
      }
      return false;
    });

    if (selectedItem) {
      if (Array.isArray(selectedItem.category)) {
        const selectedCategoryItem = selectedItem.category.find(
          (categoryItem) => categoryItem.path === selectedPath
        );
        return selectedCategoryItem ? selectedCategoryItem.component : null;
      }
      return selectedItem.component;
    }

    return null;
  };

  return (
    <Box
      className="side-bar"
      sx={{
        background: "#121212",
        minHeight: "100%",
        top: "0px",
        position: "fixed",
        bottom: "0px",
        left: "0px",
        zIndex: "1000",
      }}
    >
      <Box className="menu">
        <img
          src={Logo}
          style={{
            height: "60px",
            marginLeft: "20px",
            background: "#95cbca",
            borderRadius: "100px",
            padding: "0px 7px",
          }}
          alt=""
        />
        {/* <Profile/> */}
        <div className="profile-icon" onClick={handleAvatarClick}>
          <AccountCircleOutlinedIcon />
        </div>
        <Popover
          id={popoverId}
          open={isPopoverOpen}
          anchorEl={anchorEl}
          onClose={handleClose}
          anchorOrigin={{
            vertical: "bottom",
            horizontal: "right",
          }}
          transformOrigin={{
            vertical: "top",
            horizontal: "right",
          }}
        >
          <Box  className="pop-user"
            sx={{
              p: 2,
              display: "flex",
             
              flexDirection: "column",
              alignItems: "start",
            }}
          >
            {isPopoverOpen && (
              <div className="box-detail">
                <h2>User Details</h2>
                <div className="mail-info flex">
                  <p>Email : </p>
                  <div>{email}</div>
                </div>
                <div className="name-info flex "><p>Username :</p> <div> {username}</div></div>
                <div className="role-info flex"><p>Role :</p><div> {role}</div></div>
              </div>
            )}

            <Link to="/" className="log-out flex"  onClick={handleClose}>
              <button className=" logout-btn"><LogoutIcon />Logout</button>
            </Link>
            <Link
            className="log-out flex pwd-btn"
              to="/ChangePassword"
             
              onClick={handleClose}
            ><button className=" logout-btn"><KeyIcon/>
              Change password</button>
            </Link>
          </Box>
        </Popover>
        <List style={{ marginTop: "30px" }} component="nav">
          {renderNavItems(navItems)}
        </List>
      </Box>
    </Box>
  );
};
export default Homepage;