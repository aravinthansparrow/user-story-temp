import React, { useState, useEffect } from "react";
import Button from "@mui/material/Button";
import MenuItem from "@mui/material/MenuItem";
import Select from "@mui/material/Select";
import FormControl from "@mui/material/FormControl";
import InputLabel from "@mui/material/InputLabel";
import Dialog from "react-modal";
import "../styles/Complexity.css";
import TextField from "@mui/material/TextField";
import axios from "axios";

function Complexity() {
  const [selectedValue, setSelectedValue] = useState("");
  const [selectedDays, setSelectedDays] = useState(1);
  const [showPopup, setShowPopup] = useState(false);
  const [defaultValues, setDefaultValues] = useState({
    complexity: "",
    days: 1,
  });

  useEffect(() => {
    fetchDefaultValues();
  }, []);

  const fetchDefaultValues = async () => {
    try {
      const response = await axios.get("http://localhost:3002/complexity");
      console.log(response.data);
      setDefaultValues(response.data);
      setSelectedValue(response.data[0].complexity);
      console.log(selectedDays);
      console.log(selectedValue);
      setSelectedDays(response.data[0].days);
    } catch (error) {
      console.error(error);
    }
  };

  const handleComplexitySelect = (event) => {
    const selectedComplexity = event.target.value;
    setSelectedValue(selectedComplexity);

    // Automatically set the number of days based on the selected complexity
    switch (selectedComplexity) {
      case "Simple":
        setSelectedDays(1);
        break;
      case "Medium":
        setSelectedDays(2);
        break;
      case "Complex":
        setSelectedDays(3);
        break;
      default:
        setSelectedDays(1);
        break;
    }
  };

  const handleDaysSelect = (event) => {
    const selectedNumDays = event.target.value;
    setSelectedDays(selectedNumDays);
  };

  const handleSubmit = () => {
    axios
      .post("http://localhost:3002/complexity", {
        complexity: selectedValue,
        days: selectedDays,
      })
      .then((response) => {
        setShowPopup(true);
      })
      .catch((error) => {
        console.error(error);
      });
  };

  const handlePopupClose = () => {
    setShowPopup(false);
  };

  return (
    <div className="container">
      <p className="complexity-label">Set Complexity Level</p>
      <center>
        <FormControl className="form-control">
          <InputLabel id="complexity-select-label">
            Select Complexity
          </InputLabel>
          <Select
            id="complexity-select"
            value={selectedValue}
            onChange={handleComplexitySelect}
          >
            {defaultValues.complexity && (
              <MenuItem value={defaultValues.complexity}>
                {defaultValues.complexity}
              </MenuItem>
            )}
            <MenuItem value="Simple">Simple</MenuItem>
            <MenuItem value="Medium">Medium</MenuItem>
            <MenuItem value="Complex">Complex</MenuItem>
          </Select>
        </FormControl>
      </center>

      <FormControl className="form-control">
        <InputLabel id="days-select-label">Number of Days</InputLabel>
        <Select
          id="days-select"
          value={selectedDays}
          onChange={handleDaysSelect}
        >
          {defaultValues.days && (
            <MenuItem value={defaultValues.days}>{defaultValues.days}</MenuItem>
          )}
          <MenuItem value={1}>1</MenuItem>
          <MenuItem value={2}>2</MenuItem>
          <MenuItem value={3}>3</MenuItem>
          <MenuItem value={4}>4</MenuItem>
        </Select>
      </FormControl>

      <Button
        className="button-complex"
        variant="contained"
        color="primary"
        size="large"
        onClick={handleSubmit}
      >
        Save
      </Button>

      <Dialog
        isOpen={showPopup}
        onRequestClose={handlePopupClose}
        contentLabel="Complexity Level Modal"
        className="modal"
        overlayClassName="modal-overlay"
      >
        <div className="modal-content">
          <h2 className="modal-header">Complexity Level Set to:</h2>
          <p className="modal-body">Complexity: {selectedValue}</p>
          <p className="modal-body">Number of Days: {selectedDays}</p>
          <div className="modal-footer">
            <button className="close-button" onClick={handlePopupClose}>
              Close
            </button>
          </div>
        </div>
      </Dialog>
    </div>
  );
}

export default Complexity;
