import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import Button from "@mui/material/Button";
import TextField from "@mui/material/TextField";
import Link from "@mui/material/Link";
import Grid from "@mui/material/Grid";
import Box from "@mui/material/Box";
import Container from "@mui/material/Container";
import Joi from "joi";
import Logo from "../assets/ig-logos.png";
import { useNavigate } from "react-router-dom";
import ErrorIcon from "@mui/icons-material/Error";
import axios from "axios";
import { loginSuccess } from '../actions/authActions';
import PersonIcon from '@mui/icons-material/Person';
import LockIcon from '@mui/icons-material/Lock';
import AccountCircleIcon from '@mui/icons-material/AccountCircle';
import "../styles/Login.css"


const Login = () => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [error, setError] = useState("");

  const dispatch = useDispatch();
  

  const navigate = useNavigate();

  // Define the Joi schema for validation
  const schema = Joi.object({
    email: Joi.string().email({ tlds: false }).required(),
    password: Joi.string()
      .regex(
        /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%?&])[A-Za-z\d@$!%?&]{8,}$/

      )
      .required()
      .messages({
        "string.pattern.base":
          "Password should be alphanumeric, case-sensitive, and contain at least 1 special character.",
      }),
  });

  const handleLogin = async (e) => {
    e.preventDefault();

    try {
      
      console.log(email)
      //Validate email and password using the schema
      const { error, value } = schema.validate({ email, password });
      console.log(value);
console.log(error);
      if (error) {
        setError("Enter a valid username or password");
        return;
      }

      const response = await axios.post("http://localhost:3002/user", {
        email: email,
        password: password,
      });
      

      const { role, username,id  } = response.data;
      
      console.log(username)
      console.log(id)
      console.log(email)
     dispatch(loginSuccess(username,id, email, role));//  
         

      if (role === "developer") {
        // Redirect to developer dashboard or perform other actions
        navigate("/dashboard");
        console.log("Logged in as a developer");
      } else if (role === "admin") {
        // Redirect to admin dashboard or perform other actions
        navigate("/dashboard");
        console.log("Logged in as an admin");
      } else {
        // Invalid role
        setError("Invalid email or password");
      }
    } catch (error) {
      setError("Invalid email or password");
      console.error(error);
    }
  };

  return (
    <div className="login-component">
       <Container component="main" style={{ padding: "30px",borderRadius:"8px" ,backgroundColor:"#1a2649",position:"relative",boxShadow: "rgba(99, 99, 99, 0.2) 0px 2px 8px 0px",marginTop:"60px"}} maxWidth="xs">
      <Box
        sx={{
          
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
        }}
      >
        <img className="ig-logo" src={Logo} alt="logo" />
        <div>
          <AccountCircleIcon className="user-icon" />
        </div>
        <Box component="form" style={{width:"100%",marginTop:"20"}} onSubmit={handleLogin} sx={{ mt: 1 }}>
          <div className="input-container">
            <TextField
              type="email"
              margin="normal"
              required
              placeholder="email"
              value={email}
              onChange={(e) => setEmail(e.target.value)}
              fullWidth
              id="email"
              className="input-field"
              name="email"
              autoComplete="email"
            />
            <PersonIcon className="input-icon" />
          </div>
          <div className="input-container">
          <TextField
            placeholder="Password"
            type="password"
            className="input-field"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
            margin="normal"
            required
            fullWidth
            name="password"
            id="password"
            autoComplete="current-password"
          />
          <LockIcon  className="input-icon" />
          </div>
          {error && (
            <Box
              sx={{
                color: "red",
                display: "flex",
                alignItems: "center",
                fontWeight: "normal",
                mt: 2,
                width: "100%",
              }}
            >
              <ErrorIcon sx={{ mr: 1 }} />
              <span>{error}</span>
            </Box>
          )}
          <Button
            type="submit"
            fullWidth
            variant="contained"
            style={{background:"#00a8ab"}}
            sx={{ mt: 3, mb: 2 }}
          >
            Sign In
          </Button>
          <Grid container>
            <Grid item xs>
              <Link href="/SidenavBar/ForgotPassword" style={{color:"white",textDecoration:"none"}} variant="body2">
                Forgot password?
              </Link>
            </Grid>
          </Grid>
        </Box>
      </Box>
    </Container>
   </div>
  );
};

export default Login;