import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { useSelector, useDispatch } from 'react-redux';
import { setCreated, setApproved, setUnApproved, setRejected } from '../actions/actions';
import { useNavigate } from 'react-router-dom';
import EstimateSummary from './EstimateSummary';
import Modal from 'react-modal';
import VisibilityIcon from "@mui/icons-material/Visibility";
import '../styles/EstimateList.css'
import dayjs from 'dayjs';
import relativeTime from 'dayjs/plugin/relativeTime'; // Import the relativeTime plugin
import 'dayjs/locale/en';
import { not } from 'joi';

const SmallEstimateList = () => {
  const [clients, setClients] = useState([]);
  const [selectedClientId, setSelectedClientId] = useState(null);
  const [showEstimateSummary, setShowEstimateSummary] = useState(false);
  const [filterByDate, setFilterByDate] = useState('');
  const [filterByClientName, setFilterByClientName] = useState('');
  const [filterByEstimatedBy, setFilterByEstimatedBy] = useState('');
  const [filterByStatus, setFilterByStatus] = useState('');
  const [filteredClients, setFilteredClients] = useState([]);
  const [showFilters, setShowFilters] = useState(true);
  const [showConfirmationModal, setShowConfirmationModal] = useState(false);
  const [confirmationAction, setConfirmationAction] = useState('');
  const reduxCreated = useSelector((state) => state.created);
  const reduxApproved = useSelector((state) => state.approved);
  dayjs.extend(relativeTime); // Extend dayjs with the relativeTime plugin
  dayjs.locale('en'); // Set the locale to English

  // Dispatch the actions to update the 'created' and 'approved' values in the Redux store
  const dispatch = useDispatch();
  const navigate = useNavigate();
  useEffect(() => {
    fetchClients();
  }, []);

  useEffect(() => {
    // Count the number of rejected and not approved estimates
    const rejectedEstimates = clients.filter((client) => client.status === 'rejected');
    const notApprovedEstimates = clients.filter((client) => client.status !== 'approved');
    const approvedEstimates = clients.filter((client) => client.status === 'approved');
    const createdCount = clients.length;
    const rejectedCount = rejectedEstimates.length;
    const notApprovedCount = notApprovedEstimates.length


    // Count the number of approved estimates

    const approvedCount = approvedEstimates.length;
   


    dispatch(setCreated(createdCount));
    dispatch(setApproved(approvedCount));
    dispatch(setRejected(rejectedCount));
    dispatch(setUnApproved(notApprovedCount));
  }, [clients, dispatch]);

  const fetchClients = async () => {
    try {
      const response = await axios.get('http://localhost:3002/clients');
      const allClients = response.data;
      setClients(allClients);
      const lastFiveClients = allClients.slice(-5).reverse(); // Get the last 5 elements
      setFilteredClients(lastFiveClients);
    } catch (error) {
      console.error('Error fetching clients:', error);
    }
  };
  const handleView = (clientId) => {
    setSelectedClientId(clientId);
    setShowEstimateSummary(true);
    setShowFilters(false);
    navigate(`/estimation-list/${clientId}`);
  };

  const handleBack = () => {
    setShowEstimateSummary(false);
    setShowFilters(true);
  };


 
  const handleConfirmation = async (clientId) => {
    try {
      await axios.put(`http://localhost:3002/clients/${clientId}`, {
        status: confirmationAction,
      });
  
      // Update the status in the clients and filteredClients states
      const updatedClients = clients.map((client) => {
        if (client.id === clientId) {
          return { ...client, status: confirmationAction };
        }
        return client;
      });
  
      const updatedFilteredClients = filteredClients.map((client) => {
        if (client.id === clientId) {
          return { ...client, status: confirmationAction };
        }
        return client;
      });
  
      setClients(updatedClients);
      setFilteredClients(updatedFilteredClients);
      setShowConfirmationModal(false);
    } catch (error) {
      console.error('Error updating status:', error);
    }
  };
  
  
  
  

  return (
    <div className='recent-estimate'>

      {showEstimateSummary ? (
        <EstimateSummary clientId={selectedClientId} />
      ) : (
        <div>
          <h2>Recent Estimations</h2>
          
          <div>
            <div className="list-headers undo-justify">
            <div className="head-serial mr-29">S.No</div>
              <div className="head-date  mr-29">Date</div>
              <div className="head-name mr-29">Client Name</div>
              <div className="head-by mr-29">Estimated By</div>
              <div className="head-status mr-29">Status</div>
              <div className="head-view">View</div>
              </div>
            </div>
            <div className="list-tabs">
              {filteredClients.map((client, index) => (
                <div className="field-set estimate-new" key={client.id}>
                  <div  className="head-serial index-num">{index + 1}</div>
                  <div className="head-date">{dayjs(client.createdAt).fromNow()}</div>
                  <div className="head-name">{client.clientName}</div>
                  <div className="head-by">{client.createdBy}</div>
                  <div  className="head-status">{client.status}</div>
                
                  <div className="head-view align-l-p">
                    <button className="view-btn" onClick={() => handleView(client.id)}><VisibilityIcon /></button>
                  </div>
                </div>
              ))}
            </div>
            </div>
         
      )}
    </div>
  );
};

export default SmallEstimateList;
