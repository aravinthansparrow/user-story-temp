import React, { useState } from "react";
import {
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Paper,
  IconButton,
  Grid,
  Box,
  Button,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
} from "@mui/material";
import DeleteIcon from "@mui/icons-material/Delete";
import EditIcon from "@mui/icons-material/Edit";
import "../styles/EstimateList.css";
import "../styles/GeneralSettings.css";
import AddCircleOutlineIcon from "@mui/icons-material/AddCircleOutline";
import CloseIcon from "@mui/icons-material/Close";
import "../styles/Activity.css";

const ActivitiesPercentageSplit = () => {
  const [data, setData] = useState([
    { activity: "Requirement Analysis", percentageSplit: 2.0 },
    { activity: "Impact Analysis & Design", percentageSplit: 6.0 },
    { activity: "Development(Build & UT)", percentageSplit: 56.0 },
    { activity: "Project Management", percentageSplit: 8.0 },
    { activity: "Functional Testing", percentageSplit: 10.0 },
    { activity: "Release", percentageSplit: 2.0 },
    { activity: "Warranty", percentageSplit: 2.0 },
  ]);

  const [newActivity, setNewActivity] = useState("");
  const [newPercentage, setNewPercentage] = useState("");
  const [editIndex, setEditIndex] = useState(null);
  const [open, setOpen] = useState(false);

  const handleDelete = (index) => {
    setData((prevData) => prevData.filter((item, i) => i !== index));
  };

  const handleEdit = (index) => {
    const activity = data[index].activity;
    const percentageSplit = data[index].percentageSplit;
    setNewActivity(activity);
    setNewPercentage(percentageSplit.toString());
    setEditIndex(index);
    setOpen(true);
  };

  const handleAdd = () => {
    if (newActivity.trim() === "" || newPercentage.trim() === "") {
      // Input fields are empty, do not save
      return;
    }

    if (editIndex !== null) {
      const updatedData = [...data];
      updatedData[editIndex].activity = newActivity;
      updatedData[editIndex].percentageSplit = parseFloat(newPercentage);
      setData(updatedData);
      setEditIndex(null);
    } else {
      setData((prevData) => [
        ...prevData,
        { activity: newActivity, percentageSplit: parseFloat(newPercentage) },
      ]);
    }
    setNewActivity("");
    setNewPercentage("");
    setOpen(false);
  };

  const getTotalPercentage = () => {
    return data.reduce((total, item) => total + item.percentageSplit, 0);
  };

  const handleClose = () => {
    setNewActivity("");
    setNewPercentage("");
    setEditIndex(null);
    setOpen(false);
  };

  return (
    <Grid style={{ paddingRight: "30px" }}>
      <Grid>
        <div className="custom-popup">
        
          <Dialog open={open} onClose={handleClose}>
              <div>
                <DialogTitle
                  variant="h6"
                  className="border-flow1"
                  style={{ fontSize: "25px",background:"#1d1d1d",color:"rgb(193, 193, 193)", left: "0em" }}
                >
                  <center>Percentage Split</center>
                </DialogTitle>
              </div>
              <DialogContent  style={{background:"#1d1d1d"}} className="semi-modal border-flow2 input-custom">
                <IconButton
                  onClick={handleClose}
                  style={{
                    position: "absolute",
                    top: "8px",
                    right: "8px",
                    color: "#0af0f6",
                  }}
                >
                  <CloseIcon />
                </IconButton>
                <div className="lable" style={{background:"#1d1d1d"}}>
                  <label
                    htmlFor="activityInput"
                    style={{
                      fontWeight: "normal",
                      color: "white",
                      borderRadius: "20%",
                    }}
                  >
                    Activity:
                  </label>
                  <input
                    type="text"
                    id="activityInput"
                    required
                    value={newActivity}
                    onChange={(e) => setNewActivity(e.target.value)}
                    style={{
                      width: "70%",
                      marginBottom: "14px",
                      borderRadius: "15px",
                      fontWeight: "normal",
                      padding: "5px 5px 5px 10px",
                      border: "2px solid rgb(189, 189, 189)",
                      background:"#ffffffe3",
                      position: "relative",
                      left: "15%",
                    }}
                  />
                </div>
                <div style={{ display: "flex" }}>
                  <label
                    htmlFor="percentageInput"
                    style={{ fontWeight: "normal", color: "white" }}
                  >
                    Percentage Split:
                  </label>
                  <input
                    type="number"
                    id="percentageInput"
                    required
                    value={newPercentage}
                    onChange={(e) => setNewPercentage(e.target.value)}
                    style={{
                      width: "100%",
                      padding: "5px 5px 5px 10px",
                      marginBottom: "14px",
                      borderRadius: "15px",
                      fontWeight: "normal",
                     
                      border: "2px solid rgb(189, 189, 189)",
                      background:"#ffffffe3",
                      position: "relative",
                      left: "7%",
                    }}
                  />
                </div>
              </DialogContent>
              <DialogActions className="border-flow3"
                style={{
                  justifyContent: "right",
                  padding: "0px 36px 24px 0px",
                  background:"#1d1d1d"
                }}
              >
                <Button
                  onClick={handleClose}
                  color="primary"
                  className="custom-btns"
                >
                  Cancel
                </Button>
                <Button
                className="custom-btns"
                  onClick={handleAdd}
                  color="primary"
                >
                  {editIndex !== null ? "Update" : "Save Changes"}
                </Button>
              </DialogActions>
            
          </Dialog>
          
          <Box
            maxWidth={1200}
            maxHeight={400}
            style={{ border: "100px solid darkblack" }}
          >
            <div className="activity-title">
              <h2 style={{ color: "#c1c1c1" }}>Activities Percentage Split</h2>
              <Button onClick={() => setOpen(true)} className="add-btn">
                Add <AddCircleOutlineIcon />
              </Button>
            </div>
            <div>
              <div style={{ width: "80%", margin: "auto" }}>
                <div className="">
                  <div className="list-headers">
                    <div className="activity-head">Activity</div>
                    <div align="center" className="activity-head">
                      Percentage Split
                    </div>
                    <div align="center" className="activity-head">
                      Edit/Delete
                    </div>
                  </div>
                </div>
                <div>
                  {data.map((row, index) => (
                    <div style={{padding:"4px 10px"}} className="field-set" key={index}>
                      <div className="acttab-1">
                        {editIndex === index ? (
                          <input
                            type="text"
                            value={newActivity}
                            onChange={(e) => setNewActivity(e.target.value)}
                          />
                        ) : (
                          row.activity
                        )}
                      </div>
                      <div align="center" className="acttab-1">
                        {editIndex === index ? (
                          <input
                            type="text"
                            value={newPercentage}
                            onChange={(e) => setNewPercentage(e.target.value)}
                          />
                        ) : (
                          `${row.percentageSplit}%`
                        )}
                      </div>
                      <div
                        className="acttab-1"
                        align="center"
                        style={{ padding: "4px  0px" }}
                      >
                        {editIndex === index ? (
                          <div className="custom-icons">
                            <IconButton aria-label="update" onClick={handleAdd}>
                              {editIndex !== null ? "Update" : "Save"}
                            </IconButton>
                            <IconButton
                              aria-label="cancel"
                              onClick={handleClose}
                            >
                              <CloseIcon />
                            </IconButton>
                          </div>
                        ) : (
                          <div className="custom-icons">
                            <IconButton
                              aria-label="edit"
                              onClick={() => handleEdit(index)}
                              className="action-btn"
                            >
                              <EditIcon />
                            </IconButton>
                            <IconButton
                              aria-label="delete"
                              onClick={() => handleDelete(index)}
                              className="action-btn"
                            >
                              <DeleteIcon />
                            </IconButton>
                          </div>
                        )}
                      </div>
                    </div>
                  ))}
                  <div className="feild-tot">
                    <div colSpan={1} align="center" className="overall-tot">
                      Total
                    </div>
                    <div align="center" className="overall-tot">
                      {getTotalPercentage()}%
                    </div>
                    <div align="center"></div>
                  </div>
                </div>
              </div>
            </div>
          </Box>
        </div>
      </Grid>
    </Grid>
  );
};

export default ActivitiesPercentageSplit;
