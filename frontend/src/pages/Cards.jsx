import React, { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import { Tilt } from 'react-tilt';
import '../styles/Cards.css';
import '../styles/CardAnimations.css';
import { CircularProgressbar} from 'react-circular-progressbar';
import 'react-circular-progressbar/dist/styles.css';


const NumberCards = () => {
  const reduxCreated = useSelector((state) => state.bar.created);
  const reduxApproved = useSelector((state) => state.bar.approved);
  const reduxUnApproved = useSelector((state) => state.bar.notapproved);
  const reduxRejected = useSelector((state) => state.bar.rejected);

  const [card1Count, setCard1Count] = useState(0);
  const [card2Count, setCard2Count] = useState(0);
  const [card3Count, setCard3Count] = useState(0);
  const [card4Count, setCard4Count] = useState(0);

  const [card1Animation, setCard1Animation] = useState(false);
  const [card2Animation, setCard2Animation] = useState(false);
  const [card3Animation, setCard3Animation] = useState(false);
  const [card4Animation, setCard4Animation] = useState(false);

  useEffect(() => {
    setCard1Animation(true);
    setCard2Animation(true);
    setCard3Animation(true);
    setCard4Animation(true);
    const card1Timer = setInterval(() => {
      setCard1Count((prevCount) => prevCount + 1);
    }, 12);

    const card2Timer = setInterval(() => {
      setCard2Count((prevCount) => prevCount + 1);
    }, 14);

    const card3Timer = setInterval(() => {
      setCard3Count((prevCount) => prevCount + 1);
    }, 62);

    const card4Timer = setInterval(() => {
      setCard4Count((prevCount) => prevCount + 1);
    }, 200);

    return () => {
      clearInterval(card1Timer);
      clearInterval(card2Timer);
      clearInterval(card3Timer);
      clearInterval(card4Timer);
    };
  }, []);
  const percent1 = (reduxUnApproved/reduxCreated) * 100
  const percent2 = (reduxApproved/reduxCreated) * 100
  const percent3 = (reduxRejected/reduxCreated) * 100

  return (
    <div className="card-container">
      <Tilt className={`card card1 ${card1Animation ? 'animated slideIn' : ''}`} options={{ max: 25 }}>
        <h3>Created</h3>
        <p>{card1Count <= reduxCreated ? card1Count : reduxCreated}</p>
      </Tilt>
      <Tilt className={`card card2 ${card2Animation ? 'animated slideIn' : ''}`} options={{ max: 25 }}>
        <h3>UnApproved</h3>
        <div style={{ width: '80px', height: '80px' }}>
        <CircularProgressbar value={percent1} text={`${reduxUnApproved}`}  styles={{
    // Customize the root svg element
    root: {},
    // Customize the path, i.e. the "completed progress"
    path: {
      // Path color
      stroke: `#1d1d1d`}
      ,  text: { fill:'white',fontWeight:"700",fontSize:"22px"}}}/>
      </div>
       
       
      </Tilt>
      <Tilt className={`card card3 ${card3Animation ? 'animated slideIn' : ''}`} options={{ max: 25 }}>
        <h3>Approved</h3>
        <div style={{ width: '80px', height: '80px' }}>
        <CircularProgressbar value={percent2} text={`${reduxApproved}`}  styles={{
    // Customize the root svg element
    root: {},
    // Customize the path, i.e. the "completed progress"
    path: {
      // Path color
      stroke: `#1d1d1d`}
      ,  text: { fill:'black',fontWeight:"700",fontSize:"22px"}}}/>

        </div>
        
       
      </Tilt>
      <Tilt className={`card card4 ${card4Animation ? 'animated slideIn' : ''}`} options={{ max: 25 }}>
        <h3>Rejected</h3>
        <div style={{ width: '80px', height: '80px' }}>
        <CircularProgressbar value={percent3} text={`${reduxRejected}`}  styles={{
    // Customize the root svg element
    root: {},
    // Customize the path, i.e. the "completed progress"
    path: {
      // Path color
      stroke: `#1d1d1d`}
      ,  text: { fill:'black',fontWeight:"700",fontSize:"22px"}}}/>

        </div>
      </Tilt>
    </div>
  );
};

export default NumberCards;